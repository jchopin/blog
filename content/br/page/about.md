---
title: About me
subtitle: 
comments: false
---

My name is Julien Chopin. I am visiting professor at the Physics Institute at the Federal University of Bahia.

I am a physicist and an experimentalist interested in shape transformation of sheets and filaments, rupture of adhesion of soft materials and complex fluid flow.
