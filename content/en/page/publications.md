﻿---
title: Selected Publications
subtitle: 
comments: false
---

1. Universal scaling of the velocity field in crack front propagation, C. Le Priol, J. Chopin, P. Le Doussal, L. Ponson, A. Rosso, *Physical Review Letters* **124**, 065501 (2020)

5. Nonlinear viscoelastic modeling of adhesive failure for polyacrylate pressure-sensitive adhesives, J. Chopin, R. Villey, D. Yarusso, E. Barthel, C. Creton and M. Ciccotti, *Macromolecules* **51**, 8605 (2018)

8. Dynamic wrinkling and strengthening of an elastic filament in a viscous fluid, J. Chopin, M. Dasgupta and A. Kudrolli, *Physical Review Letters* **119**, 088001 (2017)

11. Roadmap to the morphological instabilities of a stretched twisted ribbon, J. Chopin, V. Démery, B. Davidovitch, *Journal of Elasticity* **119**, 137 (2015)

14. Helicoids, Wrinkles, and Loops in Twisted Ribbons, J. Chopin and A. Kudrolli, *Physical Review Letters* **111**, 174302 (2013)

15. Building Designed Granular Towers One Drop at a Time,  J. Chopin and A. Kudrolli, *Physical Review Letters* **107**, 208304 (2011)

16. Crack Front Dynamics across a Single Heterogeneity, J. Chopin, A. Prevost, A. Boudaoud, and M. Adda-Bedia, *Physical Review Letters* **107**, 144301 (2011)

17. The Liquid Blister Test,  J. Chopin, D. Vella and A. Boudaoud, *Proceedings of the Royal Society London A* **464**, 2887 (2008) 